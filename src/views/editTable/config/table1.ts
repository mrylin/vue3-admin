export const tabHeader = [
    {
        label: '排查描述',
        prop: 'screeningDesc',
        width: '',
        keyId: 1,
        tooltip: '这是提示'
    },
    {
        label: '隐患级别',
        prop: 'levelValue',
        width: '',
        keyId: 2,
    },
    {
        label: '排查性质',
        prop: 'screeningPropValue',
        width: '',
        keyId: 3,
    },
    {
        label: '流程状态',
        prop: 'status',
        width: '',
        keyId: 4,
        options: [
            {
                label: '待上报',
                value: 0
            },
            {
                label: '上报不通过',
                value: 1
            },
            {
                label: '待审核',
                value: 2
            },
        ],
        format: (row: any) => {
            if (row == 0) {
                return `待上报`
            } else if (row == 1) {
                return `上报不通过`
            } else if (row == 2) {
                return `待审核`
            } else {
                return `123`
            }
        }
    },
    {
        label: '类型',
        prop: 'fromType',
        width: '',
        keyId: 5,
        options: [
            {
                label: '监管平台',
                value: 0
            },
            {
                label: '企业内部',
                value: 1
            }
        ],
        format: (row: any) => {
            if (row == 0) {
                return `监管平台`
            } else if (row == 1) {
                return `企业内部`
            } else {
                return `123`
            }
        }
    },
    {
        label: '排查人',
        prop: 'screeningUserName',
        width: '',
        keyId: 7
    },
    {
        label: '排查时间',
        prop: 'screeningDate',
        width: '',
        keyId: 8
    }
]
export const tableOptions = {
    label: '操作',
    width: '300',
    //fixed: 'right',
    children: [
        {
            editState: true, //true表示正在编辑中显示的按钮
            label: '保存',
            hidden: () => true,
            size: 'default',
            methods: 'saveRowBtn'
        },
        {
            editState: true, //true表示正在编辑中显示的按钮
            label: '取消',
            btnType: 'danger',
            hidden: () => true,
            size: 'default',
            methods: 'cancelRowBtn'
        },
        {
            editState: false, //false表示未编辑中显示的按钮
            label: '编辑',
            hidden: () => true,
            btnType: 'warning',
            size: 'default',
            style: {
                marginLeft: 0
            },
            methods: 'editRowBtn'
        }
    ]
}

export const formList = [
    {
        inputType: 'input',
        prop: 'titleName',
        label: '标题名字',
    },
    {
        inputType: 'telInput',
        prop: 'phoneNumber',
        label: '手机号',
        maxlength: 11,
    },
    {
        inputType: 'select',
        prop: 'enterprise',
        label: '企业',
        options: [
            {
                value: '1',
                label: '张三',
                selectDisabled: (e: any) => {
                    return e.value == '1';
                }
            },
            {
                value: '2',
                label: '李四'
            },
        ]
    },
    {
        inputType: 'cascader',
        prop: 'company',
        label: '单位',
        configure: {
            expandTrigger: 'hover', //展开方式
            label: 'label', //指定label属性名字
            value: 'value' //指定value值名字
        },
        options: [ //数据
            {
                value: '一级1',
                label: '一级',
                children: [
                    {
                        value: '二级2',
                        label: '二级',
                    }, {
                        value: '三级3',
                        label: '三级',
                    },
                ],
            },
            {
                value: 'component',
                label: 'Component',
                children: [
                    {
                        value: 'basic',
                        label: 'Basic',
                        children: [
                            {
                                value: 'layout',
                                label: 'Layout',
                            },
                            {
                                value: 'color',
                                label: 'Color',
                            },
                            {
                                value: 'typography',
                                label: 'Typography',
                            },
                            {
                                value: 'icon',
                                label: 'Icon',
                            },
                            {
                                value: 'button',
                                label: 'Button',
                            },
                        ],
                    },
                    {
                        value: 'form',
                        label: 'Form',
                        children: [
                            {
                                value: 'radio',
                                label: 'Radio',
                            },
                            {
                                value: 'checkbox',
                                label: 'Checkbox',
                            },
                            {
                                value: 'input',
                                label: 'Input',
                            },
                            {
                                value: 'input-number',
                                label: 'InputNumber',
                            },
                            {
                                value: 'select',
                                label: 'Select',
                            },
                            {
                                value: 'cascader',
                                label: 'Cascader',
                            },
                            {
                                value: 'switch',
                                label: 'Switch',
                            },
                            {
                                value: 'slider',
                                label: 'Slider',
                            },
                            {
                                value: 'time-picker',
                                label: 'TimePicker',
                            },
                            {
                                value: 'date-picker',
                                label: 'DatePicker',
                            },
                            {
                                value: 'datetime-picker',
                                label: 'DateTimePicker',
                            },
                            {
                                value: 'upload',
                                label: 'Upload',
                            },
                            {
                                value: 'rate',
                                label: 'Rate',
                            },
                            {
                                value: 'form',
                                label: 'Form',
                            },
                        ],
                    },
                    {
                        value: 'data',
                        label: 'Data',
                        children: [
                            {
                                value: 'table',
                                label: 'Table',
                            },
                            {
                                value: 'tag',
                                label: 'Tag',
                            },
                            {
                                value: 'progress',
                                label: 'Progress',
                            },
                            {
                                value: 'tree',
                                label: 'Tree',
                            },
                            {
                                value: 'pagination',
                                label: 'Pagination',
                            },
                            {
                                value: 'badge',
                                label: 'Badge',
                            },
                        ],
                    },
                    {
                        value: 'notice',
                        label: 'Notice',
                        children: [
                            {
                                value: 'alert',
                                label: 'Alert',
                            },
                            {
                                value: 'loading',
                                label: 'Loading',
                            },
                            {
                                value: 'message',
                                label: 'Message',
                            },
                            {
                                value: 'message-box',
                                label: 'MessageBox',
                            },
                            {
                                value: 'notification',
                                label: 'Notification',
                            },
                        ],
                    },
                    {
                        value: 'navigation',
                        label: 'Navigation',
                        children: [
                            {
                                value: 'menu',
                                label: 'Menu',
                            },
                            {
                                value: 'tabs',
                                label: 'Tabs',
                            },
                            {
                                value: 'breadcrumb',
                                label: 'Breadcrumb',
                            },
                            {
                                value: 'dropdown',
                                label: 'Dropdown',
                            },
                            {
                                value: 'steps',
                                label: 'Steps',
                            },
                        ],
                    },
                    {
                        value: 'others',
                        label: 'Others',
                        children: [
                            {
                                value: 'dialog',
                                label: 'Dialog',
                            },
                            {
                                value: 'tooltip',
                                label: 'Tooltip',
                            },
                            {
                                value: 'popover',
                                label: 'Popover',
                            },
                            {
                                value: 'card',
                                label: 'Card',
                            },
                            {
                                value: 'carousel',
                                label: 'Carousel',
                            },
                            {
                                value: 'collapse',
                                label: 'Collapse',
                            },
                        ],
                    },
                ],
            },
        ]
    },
    {
        inputType: 'datePicker',
        prop: 'createDate',
        label: '创建时间',
        type: 'daterange',
        startPlaceholder: '开始时间',
        endPlaceholder: '结束时间',
        valueFormat: 'YYYY-MM-DD HH:mm', //选中后的格式化日期
        format: 'YYYY-MM-DD' //显示的格式
    },
]

export const searchBtn = [
    {
        label: '搜索',
        name: 'search',
        btnType: 'primary',
        //emit: 'searchEmit', 可以自定义点击事件名字
        isStyle: () => {

        },
    },
    {
        label: '重置',
        btnType: 'warning',
        name: 'reset',
    },
]

