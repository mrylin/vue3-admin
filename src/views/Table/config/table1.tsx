import {$error, $success} from "@/utils/message";
import {ElTag, ElButton} from 'element-plus'
import {Search} from '@element-plus/icons-vue'

export const tabHeader = [
    {
        label: '排查描述',
        prop: 'screeningDesc',
        width: '',
        keyId: 1,
        headerCellRenderer: ({column, $index}: any) => <ElButton type="primary" onClick={prompt}
                                                                 icon={Search}>{column.label}</ElButton>,
        cellRenderer: ({row, column, $index}: any) => <ElTag>{row.screeningDesc}</ElTag>
    },
    {
        label: '隐患级别',
        prop: 'levelValue',
        inactive: '0', //未打开的值
        active: '1', //打开的值
        width: '',
        keyId: 2,
        switch: true,
        changeSwitch: (e: any) => {
            if (e.screeningPropValue.includes('https')) {
                $error('当前switch不能切换')
                return false
            } else {
                return true
            }
        }
    },
    {
        label: '排查性质',
        prop: 'screeningPropValue',
        width: '',
        keyId: 3,
        image: (e: any) => e.screeningPropValue
    },
    {
        label: '流程状态',
        prop: 'status',
        width: '',
        keyId: 4,
        render: (row: any) => {
            switch (row.status) {
                case 0:
                    return `待上报`
                case 1:
                    return `<span class='blue'>上报不通过</span>`
                case 2:
                    return `<span class='purple'>待审核</span>`
            }
        }
    },
    {
        label: '类型',
        prop: 'fromType',
        width: '',
        keyId: 5,
        format: (row: any) => {
            switch (row) {
                case 0:
                    return `监管平台`
                case 1:
                    return `企业内部`
                default:
                    return `123`
            }
        }
    },
    {
        label: '排查人（点击单元可编辑）',
        prop: 'screeningUserName',
        width: '',
        keyId: 7,
        editCell: true,
        //可以把此属性的数据放在组件中遍历添加
        options: [
            {
                label: '选项一',
                value: '0'
            },
            {
                label: '选项二',
                value: '1'
            },
            {
                label: '选项三',
                value: '2'
            },
        ],
        format: (rowData: any, row: any, item: any) => item?.options?.find((item: any) => item.value == rowData)?.label
    },
    {
        label: '排查时间（点击单元可编辑）',
        prop: 'screeningDate',
        width: '',
        keyId: 8,
        editCell: true
    }
]
export const tableOptions = {
    label: '操作',
    width: '300',
    //fixed: 'right',
    children: [
        {
            hidden: () => {
                return true
            },
            label: '详情',
            methods: 'see',
            btnText: true
        },
        {
            hidden: true,
            label: '审核',
            methods: 'audit',
            btnText: true
        },
        {
            hidden: true,
            label: '流程进度',
            methods: 'schedule',
            btnText: true
        }
    ]
}

export const formList = [
    {
        inputType: 'input',
        prop: 'titleName',
        label: '标题名字',
    },
    {
        inputType: 'telInput',
        prop: 'phoneNumber',
        label: '手机号',
        maxlength: 11,
    },
    {
        inputType: 'select',
        prop: 'enterprise',
        label: '企业',
        options: [
            {
                value: '1',
                label: '张三',
                selectDisabled: (e: any) => {
                    return e.value == '1';
                }
            },
            {
                value: '2',
                label: '李四'
            },
        ]
    },
    {
        inputType: 'cascader',
        prop: 'company',
        label: '单位',
        configure: {
            expandTrigger: 'hover', //展开方式
            label: 'label', //指定label属性名字
            value: 'value' //指定value值名字
        },
        options: [ //数据
            {
                value: '一级1',
                label: '一级',
                children: [
                    {
                        value: '二级2',
                        label: '二级',
                    }, {
                        value: '三级3',
                        label: '三级',
                    },
                ],
            },
            {
                value: 'component',
                label: 'Component',
                children: [
                    {
                        value: 'basic',
                        label: 'Basic',
                        children: [
                            {
                                value: 'layout',
                                label: 'Layout',
                            },
                            {
                                value: 'color',
                                label: 'Color',
                            },
                            {
                                value: 'typography',
                                label: 'Typography',
                            },
                            {
                                value: 'icon',
                                label: 'Icon',
                            },
                            {
                                value: 'button',
                                label: 'Button',
                            },
                        ],
                    },
                    {
                        value: 'form',
                        label: 'Form',
                        children: [
                            {
                                value: 'radio',
                                label: 'Radio',
                            },
                            {
                                value: 'checkbox',
                                label: 'Checkbox',
                            },
                            {
                                value: 'input',
                                label: 'Input',
                            },
                            {
                                value: 'input-number',
                                label: 'InputNumber',
                            },
                            {
                                value: 'select',
                                label: 'Select',
                            },
                            {
                                value: 'cascader',
                                label: 'Cascader',
                            },
                            {
                                value: 'switch',
                                label: 'Switch',
                            },
                            {
                                value: 'slider',
                                label: 'Slider',
                            },
                            {
                                value: 'time-picker',
                                label: 'TimePicker',
                            },
                            {
                                value: 'date-picker',
                                label: 'DatePicker',
                            },
                            {
                                value: 'datetime-picker',
                                label: 'DateTimePicker',
                            },
                            {
                                value: 'upload',
                                label: 'Upload',
                            },
                            {
                                value: 'rate',
                                label: 'Rate',
                            },
                            {
                                value: 'form',
                                label: 'Form',
                            },
                        ],
                    },
                    {
                        value: 'data',
                        label: 'Data',
                        children: [
                            {
                                value: 'table',
                                label: 'Table',
                            },
                            {
                                value: 'tag',
                                label: 'Tag',
                            },
                            {
                                value: 'progress',
                                label: 'Progress',
                            },
                            {
                                value: 'tree',
                                label: 'Tree',
                            },
                            {
                                value: 'pagination',
                                label: 'Pagination',
                            },
                            {
                                value: 'badge',
                                label: 'Badge',
                            },
                        ],
                    },
                    {
                        value: 'notice',
                        label: 'Notice',
                        children: [
                            {
                                value: 'alert',
                                label: 'Alert',
                            },
                            {
                                value: 'loading',
                                label: 'Loading',
                            },
                            {
                                value: 'message',
                                label: 'Message',
                            },
                            {
                                value: 'message-box',
                                label: 'MessageBox',
                            },
                            {
                                value: 'notification',
                                label: 'Notification',
                            },
                        ],
                    },
                    {
                        value: 'navigation',
                        label: 'Navigation',
                        children: [
                            {
                                value: 'menu',
                                label: 'Menu',
                            },
                            {
                                value: 'tabs',
                                label: 'Tabs',
                            },
                            {
                                value: 'breadcrumb',
                                label: 'Breadcrumb',
                            },
                            {
                                value: 'dropdown',
                                label: 'Dropdown',
                            },
                            {
                                value: 'steps',
                                label: 'Steps',
                            },
                        ],
                    },
                    {
                        value: 'others',
                        label: 'Others',
                        children: [
                            {
                                value: 'dialog',
                                label: 'Dialog',
                            },
                            {
                                value: 'tooltip',
                                label: 'Tooltip',
                            },
                            {
                                value: 'popover',
                                label: 'Popover',
                            },
                            {
                                value: 'card',
                                label: 'Card',
                            },
                            {
                                value: 'carousel',
                                label: 'Carousel',
                            },
                            {
                                value: 'collapse',
                                label: 'Collapse',
                            },
                        ],
                    },
                ],
            },
        ]
    },
    {
        inputType: 'datePicker',
        prop: 'createDate',
        label: '创建时间',
        type: 'daterange',
        startPlaceholder: '开始时间',
        endPlaceholder: '结束时间',
        valueFormat: 'YYYY-MM-DD HH:mm', //选中后的格式化日期
        format: 'YYYY-MM-DD' //显示的格式
    },
]

export const searchBtn = [
    {
        label: '搜索',
        name: 'search',
        btnType: 'primary',
        //emit: 'searchEmit', 可以自定义点击事件名字
        isStyle: () => {

        },
    },
    {
        label: '重置',
        btnType: 'warning',
        name: 'reset',
    },
]
const prompt = () => $success('我是通过tsx和component渲染的')

