import {defineStore} from 'pinia'
import {ref} from 'vue'

export const useLoginStore = defineStore('loginStore', () => {
    const userName = ref<string | null>('' || localStorage.getItem('mobile'))
    const setUserName = (name: string) => {
        userName.value = name
    }
    return {userName, setUserName}
})
