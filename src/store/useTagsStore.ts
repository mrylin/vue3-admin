import {defineStore} from 'pinia'
import {computed, reactive} from 'vue'

interface tagObj {
    titleName?: string
    name?: string
    title?: string
}

interface tags {
    tags: Array<tagObj>
    tagActive: string
}

export const useTagsStore = defineStore('tagsStore', () => {
    const data = reactive<tags>({
        tags: JSON.parse(<string>sessionStorage.getItem('tag')) || [{
            titleName: 'home',
            name: '/layout/home',
            title: '首页'
        }],
        tagActive: '/layout/home'
    })

    const getTags = computed(() => data.tags)
    const getCurrentTag = computed(() => data.tagActive)
    const addTags = (item: any) => {
        data.tags.push(item)
        sessionStorage.setItem('tag', JSON.stringify(data.tags))
    }
    const addCurrentTag = (item: string) => {
        data.tagActive = item
    }
    const delTag = (obj: tagObj) => {
        data.tags.splice(data.tags.findIndex((item: tagObj) => item.name === obj.name), 1)
        sessionStorage.setItem('tag', JSON.stringify(data.tags))
    }
    const resetMenu = () => {
        sessionStorage.setItem('tag', JSON.stringify(data.tags))
    }


    return {addTags, addCurrentTag, getTags, data, getCurrentTag, delTag,resetMenu}
})
