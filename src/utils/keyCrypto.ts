// @ts-ignore
import CryptoJS from 'crypto-js';

const key = CryptoJS.enc.Utf8.parse('902F93D8B0AAEB3FC5BF80B4CA1FD6EF');  //32位十六进制数作为密钥
const iv = CryptoJS.enc.Utf8.parse('D7D458C5CFFCFB9BEA13BB44FFCD9724');   //32位十六进制数作为密钥偏移量

//解密方法
export function Decrypt(word: any) {
    let decryptHexStr = CryptoJS.enc.Hex.parse(word);
    let decryptSrc  = CryptoJS.enc.Base64.stringify(decryptHexStr);
    let decrypt = CryptoJS.AES.decrypt(decryptSrc, key, {iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7});
    let decryptedStr = decrypt.toString(CryptoJS.enc.Utf8);
    return decryptedStr.toString();
}

//加密方法
export function Encrypt(word: any) {
    let encryptSrc = CryptoJS.enc.Utf8.parse(word);
    let encrypted = CryptoJS.AES.encrypt(encryptSrc, key, {iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7});
    return encrypted.ciphertext.toString().toUpperCase();
}

