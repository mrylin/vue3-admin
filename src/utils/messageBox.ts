import {ElMessageBox} from 'element-plus'
//定义类型只能是这几个名称
type MessageType = '' | 'success' | 'warning' | 'info' | 'error'

export function $msgBox(msg: string, title: string, msgType: MessageType = 'warning',
                        confirmButtonText: string = '确定', cancelButtonText: string = '取消') {
    return new Promise((resolve, reject) => {
        ElMessageBox.confirm(msg, title, {
            distinguishCancelAndClose: true,
            dangerouslyUseHTMLString: true,
            confirmButtonText: confirmButtonText,
            cancelButtonText: cancelButtonText,
            type: msgType
        }).then((success) => {
            resolve(success)
        }).catch((action) => {
            if (action == 'cancel') {
                //取消
                reject(action)
            } else {
                reject('close')
            }
        })
    })
}
