import {flatTree} from "@/utils/treeTool";
import {dataMenu} from "@/menu";
import {Decrypt, Encrypt} from "@/utils/keyCrypto";


export function setMenu(): void {
    localStorage.setItem(Encrypt('menu'), Encrypt(JSON.stringify(flatTree(dataMenu))))
}

export const isAuthority = (path: string): boolean => {
    const whitelist = ['/login', '/404', '/403']
    //不等于登录页和404页的才进入判断是否有权限。等于的话直接放行
    if (!whitelist.includes(path)) {
        const authority = localStorage.getItem(Encrypt('menu'))
        if (authority) {
            const arr = JSON.parse(Decrypt(authority))
            console.time('权限')
            const have = arr.filter((item: { path: string; }) => item.path == path)
            console.timeEnd('权限')
            //有权限返回false。没有true
            return have.length <= 0;
        }
    }
    return false
}
