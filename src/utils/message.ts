import {ElMessage} from 'element-plus'

const duration = 2000


export function $success(msg: string) {
    ElMessage({
        duration: duration,
        type: 'success',
        message: msg
    })
}

export function $error(msg: string, center: boolean = false, showClose: boolean = false) {
    ElMessage({
        duration: duration,
        type: 'error',
        message: msg,
        center: center,
        showClose: showClose
    })
}

export function $warning(msg: string) {
    ElMessage({
        duration: duration,
        type: 'warning',
        message: msg,
    })
}
