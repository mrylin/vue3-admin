//找出两个数组不同的值
/*const list = [1, 6, 3, 4, 2, 8, 5]
const list2 = [1, 5, 3, 2]
const list3 = [1, 3, 5]
const list4 = [1, 2, 4]
console.log(getArrDifference(list, list2)); //[6, 4, 8]
console.log(getArrDifference(list3, list4));//[3, 5, 2, 4]*/
export const getArrDifference = (before:Array<any>, after:Array<any>) => before.concat(after).filter((v, i, currArr) => currArr.indexOf(v) === currArr.lastIndexOf(v))
