const dynamicLoadScript = (src: string) => {
    return new Promise((resolve: any, reject: any) => {
        const existingScript = document.getElementById(src);
        if (existingScript) resolve();
        if (!existingScript) {
            const script = document.createElement("script");
            script.src = src;
            script.id = src;
            document.body.appendChild(script);
            script.onload = resolve;
            script.onerror = reject;
        }
    });
};

export default dynamicLoadScript;