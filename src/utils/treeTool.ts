//装树
/**
 *
 * @param arr
 * @param pid //最顶层的pid
 */
export function arrToTree(arr: Array<any>, pid = null) {
    const res: Array<any> = []
    arr.forEach(item => {
        if (item.pid === pid) {
            const children = arrToTree(arr.filter(v => v.pid !== pid), item.id)
            if (children.length) {
                res.push({
                    ...item,
                    children
                })
            } else {
                res.push({...item})
            }
        }
    })
    return res
}

//平铺树
export function flatTree(data: Array<object>, pid = null, res: Array<object> = []) {
    data.forEach((item: any) => {
        res.push({...item, pid: pid});
        if (item.children && item.children.length !== 0) {
            flatTree(item.children, item.id, res)
        }
    })
    return res
}

//向上递归树
export const treeFindPath = (tree: any, func: any, path: any = []): any => {
    if (!tree) return [];
    for (const data of tree) {
        // 这里按照你的需求来存放最后返回的内容吧
        path.push(data.id);
        if (func(data)) return path;
        if (data.children) {
            const findChildren = treeFindPath(data.children, func, path);
            if (findChildren.length) return findChildren;
        }
        path.pop();
    }
    return [];
};
const res = [{
    code: 1,
    name: "湖北省",
    children: [{
        code: 1,
        name: "武汉市",
        children: [{
            code: 1,
            name: "汉阳区",
            children: [{
                code: 1,
                name: "水上分局"
            }]
        }, {
            code: 1,
            name: "武昌区",
            children: [{
                code: 1,
                name: "水上分局"
            }]
        }, {
            code: 1,
            name: "汉口区",
            children: [{
                code: 1,
                name: "水上分局"
            }]
        }]
    }, {
        code: 1,
        name: "十堰市",
        children: [{
            code: 1,
            name: "郧阳区",
            children: [{
                code: 1,
                name: "安阳镇"
            }]
        }, {
            code: 1,
            name: "茅箭区",
            children: [{
                code: 1,
                name: "小川乡"
            }]
        }]
    }]
}]
treeFindPath(res, (data:any)=> data.name==='水上分局')

