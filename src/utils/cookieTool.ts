// @ts-ignore
import Cookies from 'js-cookie'

let num = 30 //失效时间是分钟

export function setCookie(name: string, value: any) {
    let time = new Date(new Date().getTime() + num * 60 * 1000); //分钟
    //let time = new Date(new Date().getTime() + num * 60 * 60 * 1000); //小时
    console.log("失效", time)
    //不传date默认过期时间为30分钟
    return Cookies.set(name, value, {expires: time})
}

export function getCookie(name: string): any {
    let CookieCont = Cookies.get(name)
    if (CookieCont) {
        return CookieCont
    } else {
        return false;
    }
}

export function delCookie(name: string) {
    return Cookies.remove(name)
}
