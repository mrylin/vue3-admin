import type {RouteRecordRaw} from "vue-router";

export const basicArr = [
    {
        path: '/layout',
        name: 'layout',
        component: () => import('@/layout/index.vue'),
        redirect: '/layout/basicsTable',
        children: [
            {
                path: 'home',
                name: 'home',
                meta: {title: '首页'},
                component: () => import('@/views/home/index.vue')
            },
            {
                path: 'tableV2',
                name: 'tableV2',
                meta: {title: '虚拟表格示例'},
                component: () => import('@/views/tablev2/tableV2.vue')
            },
            {
                path: 'basicsTable',
                name: 'basicsTable',
                meta: {title: 'el-table示例'},
                component: () => import('@/views/Table/basicsTable.vue')
            },
            {
                path: 'basicsEditTable',
                name: 'basicsEditTable',
                meta: {title: '行编辑表格示例'},
                component: () => import('@/views/editTable/basicsEditTable.vue')
            },
            {
                path: 'menu',
                name: 'menu',
                meta: {title: '菜单'},
                component: () => import('@/views/menu/redirect.vue'),
                children: [
                    {
                        path: 'menu1',
                        name: 'menu1',
                        meta: {title: '菜单2实例'},
                        component: () => import('@/views/menu/menu1/index.vue')
                    },
                ]
            },
            {
                path: 'tableHeader',
                name: 'tableHeader',
                meta: {title: '表头示例'},
                component: () => import('@/views/menu/index.vue')
            }
        ]
    },
    {
        path: '/login',
        name: 'login',
        meta: {title: '登录'},
        component: () => import('@/views/login.vue'),
    },
    {
        path: '/',
        redirect: '/layout',
    },
    {
        path: '/404',
        name: '404',
        meta: {title: '404'},
        component: () => import('@/views/error/404.vue'),
    },
    {
        path: '/403',
        name: '403',
        meta: {title: '403'},
        component: () => import('@/views/error/404-1.vue'),
    },
    {
        path: "/:catchAll(.*)",
        redirect: '/404'
    }
]
