import {createRouter, createWebHistory, RouteRecordRaw, createWebHashHistory} from 'vue-router'
import {storeToRefs} from "pinia";
import pinia from '@/store'
import {useTagsStore} from "@/store/useTagsStore";
import {basicArr} from './modules/basic'
// @ts-ignore
import NProgress from 'nprogress'
import {whiteArr} from "./whiteArr";

import {getCookie} from "@/utils/cookieTool";
import {isAuthority} from "@/utils/authority";


const store = useTagsStore(pinia)

const {data} = storeToRefs(store);
const {getTags, addTags, addCurrentTag} = store;

const routes: Readonly<RouteRecordRaw[]> = [
    ...basicArr
]

const router = createRouter({
    history: createWebHashHistory(),
    routes
})
router.beforeEach((to, from, next) => {
// 根据菜单判断用户有没有当前页面权限
    if (isAuthority(to.path)) {
        next({name: '404', replace: true})
    }


    // 路由切换显示加载弹出层
    loadingState(true)

    NProgress.start()

    //面包屑tag
    let newArr: number //判断当前路径是否在白名单中
    if (!whiteArr.includes(to.name)) {
        addCurrentTag(to.path)
        newArr = data.value.tags.findIndex(v => v.name == to.path);
        if (newArr == -1) {
            if (to.name != 'home') {
                addTags({titleName: to.name, name: to.path, title: to.meta.title})
            }
        }
    }


    //动态设置标题
    if (to.meta.title) {//判断是否有标题
        const doc = document as any
        doc.title = to.meta.title
    }
    //判断cookie是否存在
    to.name == 'login' ? next() : getCookie('adminToken') ? next() : next({name: 'login', replace: true})
})
router.afterEach((to, from) => {
    NProgress.done()
    loadingState(false)
})

// 删除/重置路由
export function resetRoute(): void {
    router.getRoutes().forEach((route) => {
        const {name} = route
        if (name) {
            router.hasRoute(name) && router.removeRoute(name)
        }
    })
}

const loadingState = (state: boolean) => {
    const doc = document as any
    const domModal = doc.querySelector('#load-modal')
    domModal.style.display = state ? 'block' : 'none'
}

export default router
