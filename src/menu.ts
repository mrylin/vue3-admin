export const dataMenu = [{
    name: '顶层',
    id: 0,
    pid: null,
    children: [
        {
            icon: 'aichegujiabeifen7',
            name: '首页',
            path: '/layout/home',
            id: 9,
            pid: 0,
            children: []
        },
        {

            icon: 'aichegujiabeifen',
            name: 'el-table-v2',
            path: '',
            id: 1,
            pid: 0,
            children: [
                {
                    icon: 'aichegujiabeifen7',
                    name: '虚拟表格示例',
                    path: '/layout/tableV2',
                    id: 2,
                    pid: 1,
                    children: []
                },
            ]
        },
        {
            id: 2,
            pid: 0,
            icon: 'maichepin',
            name: 'el-table',
            path: '',
            children: [
                {
                    id: 1,
                    pid: 2,
                    icon: 'weixiu',
                    name: 'el-table示例',
                    path: '/layout/basicsTable',
                    children: []
                },
            ]
        },
        {
            id: 3,
            pid: 0,
            icon: 'wodekanjia',
            name: '行编辑表格',
            path: '',
            children: [
                {
                    id: 1,
                    pid: 3,
                    icon: 'xinchebaojia',
                    name: '编辑示例',
                    path: '/layout/basicsEditTable',
                    children: []
                },
            ]
        },
        {
            id: 4,
            pid: 0,
            icon: 'youkachongzhi',
            name: '多级表头',
            path: '',
            children: [
                {
                    id: 1,
                    pid: 4,
                    icon: 'zhuanjiadayibeifen',
                    name: '菜单1',
                    path: '',
                    children: [
                        {
                            id: 2,
                            pid: 4,
                            icon: 'zhuanjiadayibeifen',
                            name: '菜单2实例',
                            path: '/layout/menu/menu1',
                            children:[]
                        }
                    ]
                },
                {
                    id: 3,
                    pid: 4,
                    icon: 'zhuanjiadayibeifen',
                    name: '表头示例',
                    path: '/layout/tableHeader',
                    children:[]
                }
            ]
        },
    ]
}]
