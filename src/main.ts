import {createApp} from 'vue';
import App from './App.vue';
import router from './router';
import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
//全局样式
import './assets/css/index.scss'

// @ts-ignore
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import 'virtual:svg-icons-register';
import * as _lodash from 'lodash'
import {$error, $success, $warning} from "@/utils/message";

//引入自定义颜色覆盖element-ui的主题色
import './assets/css/element-variables.css'
import pinia from "@/store"

const app = createApp(App)
app.use(router).use(pinia).use(ElementPlus)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}

app.config.globalProperties.$lodash = _lodash
app.config.globalProperties.$error = $error
app.config.globalProperties.$success = $success
app.config.globalProperties.$warning = $warning
app.mount('#app')


