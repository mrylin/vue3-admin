type  buttonType = 'primary' | 'success' | 'warning' | 'danger' | 'info' | 'text'
type  sizeType = 'large' | 'default' | 'small'
type fixed = true | 'left' | 'right' //列是否固定在左侧或者右侧。 true 表示固定在左侧

export interface tableConfigInterface {
    showHeader: boolean
    highlightCurrentRow: boolean
    tooltipEffect: string
    maxHeight: string | number
    emptyText: string
    elementLoadingText: string
    headerCellStyle: {
        background: string
        color: string
        fontSize: string
    },
    border: boolean
    fit: boolean
    stripe: boolean
}

export interface tableLabels {
    children?: Array<tableLabels> //子节点
    tooltip?: string //提示文字
    width?: string | number //列宽
    keyId?: string | number //列key
    align?: string //对齐方式
    label?: string //名字
    fixed?: string //固定位置
    overflowText?: boolean //溢出隐藏
    prop?: string //变量名
    image?: Function //图片url函数
    progress?: boolean //进度条
    switch?: boolean //开关
    editCell?: boolean //是否支持编辑
    active?: boolean | string | number //开关打开值
    inactive?: boolean | string | number //开关关闭值
    changeSwitch?: Function //开关切换事件。返回 false 或者返回 Promise的reject，则停止切换。
    render?: Function //格式化html函数
    methods?: Function //自定义方法
    format?: Function //格式化字符串函数
    options?: Array<selectOptions>
    headerCellRenderer?: Function //自定义头部组件
    cellRenderer?: Function //自定义单元组件
}

interface selectOptions {
    value?: sizeType //下拉的值
    label?: string //下拉名字
}

export interface btnArray {
    btnType?: buttonType //按钮类型
    btnText?: string //文字按钮
    methods?: string //自定义按钮事件名
    size?: sizeType //按钮大小
    label?: string //按钮名字
    editState?: Function | boolean //编辑状态
    style?: Function //按钮样式
    hidden?: Function //按钮显示隐藏
    disabled?: Function //按钮是否禁用
}

export interface tableOption {
    label: string //操作按钮列名字
    width?: string //宽度
    fixed?: fixed
    children?: Array<btnArray>
}

export interface paginationOption {
    pageIndex: number
    totalCount: number
    pageSize: number
    pageArr?: Array<number>
}

export interface clickBtnParam {
    methods?: string
    row?: object
    index?: number
    rowIndex?: number

}
