type size = 'large' | 'default' | 'small'

export  interface PropsParams {
    params: any

    [key: string]: any
}

export interface searchList {
    inputType: string //筛选框类型
    prop: string //v-mode属性名
    width?: string | number //el-form-item宽度
    label: string | number //el-form-item名称
    maxlength?: string | number //el-input最大长度
    minlength?: number | string //el-input最小长度
    placeholder?: string //输入框占位文本
    clearable?: boolean //是否显示清除按钮
    disabled?: boolean //是否禁用
    size?: size //输入框尺寸
    options?: any //数据
    configure?: any //配置
    type?: string //日期组件类型
    startPlaceholder?: string //开始日期占位符
    endPlaceholder?: string //结束日期占位符
    valueFormat?: string //选中后的格式化日期
    format?: string //显示的格式
    multiple: boolean //select是否多选
    attrs?: any //可以自定义传入其他属性
}
