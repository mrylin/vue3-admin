import axios from 'axios'
// @ts-ignore
import NProgress from 'nprogress'
import {getCookie} from '@/utils/cookieTool'
import {$error} from "@/utils/message";

// '/api'指向代理的名称
const instance = axios.create({
    baseURL: import.meta.env.MODE === 'development' ? '/api' : import.meta.env.VITE_BASE_URL,
    timeout: 30000,
    headers: {
        'Content-Type': 'application/json;charset=UTF-8',
    }
});


const keyMap = new Map(); // 保存唯一的key

//获取唯一标识
const generateRequestKey = (config: any) => {
    const {url, method, params, data} = config;
    return [url, method, JSON.stringify(params), JSON.stringify(data)].join('&');
}

//添加请求
const addPendingRequest = (config: any) => {
    const key = generateRequestKey(config);
    if (!keyMap.has(key)) {
        config.cancelToken = new axios.CancelToken(cancel => keyMap.set(key, cancel))
    }
}

//删除重复请求
const removePendingRequest = (config: any) => {
    const key = generateRequestKey(config);
    if (keyMap.has(key)) {
        const cancelToken = keyMap.get(key);
        cancelToken('已取消请求');
        keyMap.delete(key);
    }
}
// 添加请求拦截器
instance.interceptors.request.use(function (config: any) {
    NProgress.start()
    // 在发送请求之前做些什么
    removePendingRequest(config);
    addPendingRequest(config)
    config.headers["assToken"] = getCookie('adminToken') || ''
    console.log("请求", config)
    return config;
}, function (error: any) {
    // 对请求错误做些什么
    return Promise.reject(error);
});

// 添加响应拦截器
instance.interceptors.response.use(function (response: any) {
    NProgress.done()
    // 对响应数据做点什么，可以进行解密,判断登录状态

    //请求回来了删除当前请求
    removePendingRequest(response.config);

    console.log("响应", response)
    return response;
}, function (error: any) {
    NProgress.done()
    // 对响应错误做点什么
    if (!(error?.response?.status)) {
        $error(error?.message)
    } else {
        switch (error?.response?.status) {
            case 400:
                error.response.message = '请求错误(400)'
                break
            case 401:
                error.response.message = '未授权，请重新登录(401)'
                break
            case 404:
                error.response.message = '没有找到指定的资源(404)'
                break
            case 408:
                error.response.message = '请求超时(408)'
                break
            case 500:
                error.response.message = '服务器错误(500)'
                break
            case 501:
                error.response.message = '服务器不支持请求的函数(501)'
                break
            case 502:
                error.response.message = '服务器网络错误(502)'
                break
            case 503:
                error.response.message = '服务不可用(503)'
                break
            case 504:
                error.response.message = '网络超时(504)'
                break
            case 505:
                error.response.message = 'HTTP版本不受支持(505)'
                break
            default:
                error.response.message = `未知错误：${error?.response?.status}`
        }
        $error(error.response.message)
    }
    return Promise.reject(error);
});


export function get(url: string, params?: object): object {
    return new Promise((resolve, reject) => {
        instance.get(url, {params}).then((res: any) => {
            resolve(res)
        }).catch((err: any) => {
            reject(err)
        })
    })
}

export function post(url: string, data?: object): object {
    return new Promise((resolve, reject) => {
        instance.post(url, data).then((res: any) => {
            resolve(res)
        }).catch((err: any) => {
            reject(err)
        })
    })
}

//下载无进度
export function downloadFile(url: string, data?: object): object {
    return new Promise((resolve, reject) => {
        instance({
            method: 'get',
            url,
            params: data,
            responseType: 'blob',
        }).then(response => {
            //现在这个下载的文件流式放在data属性中的，如果直接是一个流就resolve(response)
            const res = response.data
            const filename = decodeURI(res.headers['content-disposition']?.split(';')[1].split('filename=')[1])
            //返回文件流和文件名
            resolve({res, filename})
        }).catch(err => {
            reject(err)
        })
    })
}


//下载有进度
/**
 *
 * @param url
 * @param data
 * @param config
 *
 downloadSedarch(url,data,{
        onDownloadProgress:progress=> {
          console.log(Math.round(progress.loaded / progress.total * 100) + '%');
        }
      }).then(res=>{

      })
 })*/


export function downloadFileProgress(url: string, data?: object, config?: object): object {
    return instance({
        method: 'get',
        url,
        params: data,
        ...config,
        responseType: 'blob'
    })
}

//上传无进度
export function uploadFile(url: string, files?: object | Array<object>): object {
    return new Promise((resolve, reject) => {
        instance.post(url, files, {headers: {'Content-Type': 'multipart/form-data'}}
        ).then(res => {
            resolve(res)
        }).catch(err => {
            reject(err)
        })
    })
}

//上传有进度
/**
 *
 * @param url
 * @param files
 * @param config
 * uploadFileProgress(url,data,{
    onUploadProgress:progress=>{
        console.log(Math.round(progress.loaded / progress.total * 100) + '%');
    }
}).then(res=>{

})
 */
export function uploadFileProgress(url: string, files: object | Array<object>, config: any): object {
    return instance.post(url, files, {headers: {'Content-Type': 'multipart/form-data'}, ...config})
}
