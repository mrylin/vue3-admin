import {defineConfig} from 'vite'
import vue from '@vitejs/plugin-vue'
import Components from 'unplugin-vue-components/vite'
import {ElementPlusResolver} from 'unplugin-vue-components/resolvers'
import Icons from 'unplugin-icons/vite'
import IconsResolver from 'unplugin-icons/resolver'
import {createSvgIconsPlugin} from 'vite-plugin-svg-icons'
import vueJsx from '@vitejs/plugin-vue-jsx'
import * as path from "path";
import {base_url} from './src/apiConfig'

export default defineConfig({
  resolve: {
    alias: [{
      find: '@',
      replacement: path.resolve(__dirname, 'src')
    }
    ],
  },
  base: './',
  plugins: [vue(),
    vueJsx(),
    Components({
      resolvers: [
        // 自动导入element-plus组件
        ElementPlusResolver(),
        // 自动导入图标组件
        IconsResolver({
          enabledCollections: ['ep']
        }),
      ],
    }), createSvgIconsPlugin({
      iconDirs: [path.resolve(process.cwd(), 'src/assets/svg')],
      symbolId: 'icon-[dir]-[name]'
    }),
    // 自动导入图标组件
    Icons({
      autoInstall: true,
    })],
  server: {
    host: '0.0.0.0',
    port: 9999,
    proxy: {
      // 使用 proxy 实例
      '/api': {
        target: base_url.dev,
        changeOrigin: true,
        ws: true,
        rewrite: (path: any) => path.replace(/^\/api/, '')
      }
    },
  },
})
